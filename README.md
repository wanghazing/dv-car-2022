# 2022 中国乘用车销量数据可视化

[在线预览](http://159.75.210.117:8033/dv1/)

支持修改图标边框颜色、粗细
支持缩放平移

# 依赖

[sevlte](https://svelte.dev/)
[tailwind](https://tailwindcss.com/)
